import React from 'react';


import {
    Button, Col,
    Input, Row
} from 'reactstrap';

import Table from "../../commons/tables/table";
import * as API_USERS from "../api/caregiver-api";


class CaregiverPatientsTable extends React.Component {


    constructor(props) {
        super(props);
        this.dataInfo = [];
        this.reloadHandler = this.props.reloadHandler;
        this.id = this.props.id;
        this.state = {
            patientsList: this.props.patientsList
        };


    }
    // this.handleSubmit = this.handleSubmit.bind(this);

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
            Width: 150
        },
        {
            Header: 'Address',
            accessor: 'address',
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
        },
        {
            Header: 'Gender',
            accessor: 'gender',
        },
        {
            Header: 'Username',
            accessor: 'username',
        },
        {
            Header: 'Password',
            accessor: 'password',
        },
        {
            Header: 'MedicalRecord',
            accessor: 'medicalRecord',
        }];
    filters = [
        {
            accessor: 'name',
        }
    ];


    getById(id) {
        return API_USERS.getCaregiverById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get caregiver with id: " + id);
                this.dataInfo[0] = result.id;
                this.dataInfo[1] = result.name;
                this.dataInfo[2] = result.address;
                this.dataInfo[3] = result.birthdate;
                this.dataInfo[4] = result.gender;
                this.dataInfo[5] = result.username;
                this.dataInfo[6] = result.password;
                this.dataInfo[7] = result.listOfPatients;
                console.log("The patients are: "+ result.listOfPatients)


            }
            else console.log("failed with id: "+ id);



        });


    }

    render() {
        return (

                <div className="tableClass2">
                    <Table className = "tableDetails2"
                        data={this.state.patientsList}
                        columns={this.columns}
                        search={this.filters}
                        pageSize={5}
                    />

                </div>



        )
    }


}

export default CaregiverPatientsTable;