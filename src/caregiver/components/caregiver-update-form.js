import React from 'react';
import validate from "./validators/caregiver-validators";
//import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label, Button} from 'reactstrap';
import * as API_USERS_A from "../../patient/api/patient-api";
import Table from "../../commons/tables/table";
import moment from "moment";


class CaregiverUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;
        this.id = this.props.id;
        this.dataInfo = this.props.dataInfo;
        this.patientsList = this.props.patientList;
        this.patientUsername = [];
        this.state = {


            errorStatus: 0,
            error: null,
            selected: {},
            selectAll:0,
            formIsValid: false,

            formControls: {
                name: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                address: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                birthdate: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false
                },
                gender: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1
                    }
                },
                username: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },
                password: {
                    value: '',
                    placeholder: '',
                    valid: false,
                    touched: false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.editCaregiver = this.editCaregiver.bind(this);
        this.toggleRow = this.toggleRow.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };



    getPatientUsername(id) {
        return API_USERS_A.getPatientByUsername(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + id);

                let i = this.patientUsername.length;
                this.patientUsername[i] = result;

            }
            else console.log("failed with id: "+ id);
        });
    }

    toggleRow(username) {
        const newSelected = Object.assign({}, this.state.selected);
        newSelected[username] = !this.state.selected[username];

        console.log("info "+ username);

        //this.getPatientById(username);
        this.getPatientUsername(username);

        this.setState({
            selected: newSelected,
            selectAll: 2
        });
    }

    toggleSelectAll() {
        let newSelected = {};

        if (this.state.selectAll === 0) {
            this.state.patientsList.forEach(x => {
                newSelected[x.username] = true;
            });
        }

        this.setState({
            selected: newSelected,
            selectAll: this.state.selectAll === 0 ? 1 : 0
        });
    }

    columns = [
        {
            Header: '',
            columns: [
                {
                    id: "checkbox",
                    accessor: "",
                    Cell: ({ original }) => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selected[original.username] === true}
                                onChange={() => this.toggleRow(original.username)}
                            />
                        );
                    },
                    Header: x => {
                        return (
                            <input
                                type="checkbox"
                                className="checkbox"
                                checked={this.state.selectAll === 1}
                                ref={input => {
                                    if (input) {
                                        input.indeterminate = this.state.selectAll === 2;
                                    }
                                }}
                                onChange={() => this.toggleSelectAll()}
                            />
                        );
                    },
                    sortable: false,
                    width: 45
                }]
        },
        {
            Header: 'Name',
            accessor: 'name',


        },
        {
            Header: 'Address',
            accessor: 'address',

        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',

        },
        {
            Header: 'Gender',
            accessor: 'gender',

        },
        {
            Header: 'Username',
            accessor: 'username',

        },
        {
            Header: 'Password',
            accessor: 'password',

        },
        {
            Header: 'MedicalRecord',
            accessor: 'medicalRecord',

        }
    ];

    filters = [

    ];


    editCaregiver(caregiver) {
        return API_USERS.updateCaregiver(caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted caregiver with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {

        for(let i = 0; i<this.patientUsername.length; i++)
        {
            this.patientUsername[i].birthdate = moment(this.patientUsername[i].birthdate).format('MM.DD.YYYY');
        }


        let caregiver = {
            id: this.id,
            name: this.state.formControls.name.value,
            address: this.state.formControls.address.value,
            birthdate: this.state.formControls.birthdate.value,
            gender: this.state.formControls.gender.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            listOfPatients: this.patientUsername

        };

        console.log(caregiver);
        this.editCaregiver(caregiver);
    }

    render() {

        return (
            <div>

                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.dataInfo[1]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                    {this.state.formControls.name.touched && !this.state.formControls.name.valid &&
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>}
                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.dataInfo[2]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='birthdate'>
                    <Label for='birthdateField'> Birthdate: </Label>
                    <Input name='birthdate' id='birthdateField' placeholder={this.dataInfo[3]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.birthdate.value}
                           touched={this.state.formControls.birthdate.touched? 1 : 0}
                           valid={this.state.formControls.birthdate.valid}
                           required
                    />
                    {this.state.formControls.birthdate.touched && !this.state.formControls.birthdate.valid &&
                    <div className={"error-message"}> * Birthdate must have a valid format</div>}
                </FormGroup>


                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.dataInfo[4]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder={this.dataInfo[5]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                           valid={this.state.formControls.username.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.dataInfo[6]}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                           valid={this.state.formControls.password.valid}
                           required
                    />
                </FormGroup>

                <div>
                    <Row >
                        <Col sm={{size: '9', offset: 0}}>
                            <Table
                                data={this.patientsList}
                                columns={this.columns}
                                search={this.filters}
                                pageSize={5}

                            />
                        </Col>
                    </Row>
                </div>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Update </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default CaregiverUpdateForm;
