import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Home from './home/home';

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import PatientContainer from "./patient/patient-container";
import MedicationContainer from "./medication/medication-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import Login from "./login/login-page";
import PatientPage from "./login/patient-page"
import CaregiverPage from "./login/caregiver-page"
import BackgroundImg from "./commons/images/med10.jpg";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1200px",
    backgroundImage: `url(${BackgroundImg})`,

};

class App extends React.Component {


    render() {

        return (
            <div style={backgroundStyle} className={"backgroundFull"}>
            <Router>
                <div>

                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Login/>}
                        />

                        <Route
                            exact
                            path='/doctor'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/patient-page'
                           component={PatientPage}/>}
                        />

                        <Route
                            exact
                            path='/caregiver-page'
                            component={CaregiverPage}/>}
                        />

                        <Route
                            exact
                            path='/doctor/patient'
                            render={() => <PatientContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/medication'
                            render={() => <MedicationContainer/>}
                        />

                        <Route
                            exact
                            path='/doctor/caregiver'
                            render={() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <Login/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route render={() =><Login/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
