import React from "react";
import Table from "../../commons/tables/table";
import * as API_USERS from "../../medication/api/medication-api";



class MedicationTable extends React.Component {

    constructor(props) {
        super(props);
        this.reloadHandler = this.props.reloadHandler;
        this.valid = false;
        this.state = {
            tableData: this.props.tableData
        };
    }

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'SideEffects',
            accessor: 'sideEffects',
        },
        {
            Header: 'Dosage',
            accessor: 'dosage',
        },
        {
            Header: 'Delete',
            accessor: 'id',
            Cell:cell=>(<button className="deleteButton" onClick={()=> this.handleDelete(cell.value)}> Delete </button>)
        },
        {
            Header: 'Update',
            accessor: 'id',
            Cell:cell=>(<button className="updateButton" onClick={()=> this.handleById(cell.value)}> Update </button>)
        }
    ];

    filters = [
        {
            accessor: 'name',
        }
    ];

    handleById(id) {
        this.props.idValue(id);
        return id;
    }

    handleDelete(id) {
        return API_USERS.deleteMedication(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted medication with id: " + result);
                this.reloadHandler();
            }
        });

    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={this.filters}
                pageSize={5}
                onChange={this.handleById}
            />
        )
    }
}

export default MedicationTable;