import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import MedicationForm from "./components/medication-form";

import * as API_USERS from "./api/medication-api"
import MedicationTable from "./components/medication-table";
import MedicationUpdateForm from "./components/medication-update-form";



class MedicationContainer extends React.Component {
    dataInfo = [];
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.reloaded = this.reloaded.bind(this);
        this.state = {
            id: "",
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedication();
    }

    fetchMedication() {
        return API_USERS.getMedication((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleUpdateById(id) {
        return API_USERS.getMedicationById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get medication with id: " + id);
                this.dataInfo[0] = result.id;
                this.dataInfo[1] = result.name;
                this.dataInfo[2] = result.sideEffects;
                this.dataInfo[3] = result.dosage;

            }
            else console.log("failed with id: "+ id);
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedication();
    }

    reloaded() {
        this.setState({
            isLoaded: false
        });
        this.fetchMedication();
    }

    handleById = (id) => {
        this.toggleForm();
        this.state.id = id;
        this.validUpdate();
        this.handleUpdateById(id);
        console.log("This is the id: " + id);

    }

    validUpdate(){
        this.validate=true;
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Medication Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medication </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData} idValue = {this.handleById} reloadHandler = { this.reloaded }/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>

                        {(this.validate)?<MedicationUpdateForm reloadHandler={this.reload} id = {this.state.id} dataInfo = {this.dataInfo} /> : <MedicationForm reloadHandler={this.reload}/> }
                        {this.validate = false}
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default MedicationContainer;
