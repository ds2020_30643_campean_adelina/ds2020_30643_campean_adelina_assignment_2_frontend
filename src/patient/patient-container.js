import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PatientForm from "./components/patient-form";

import * as API_USERS from "./api/patient-api"
import PatientTable from "./components/patient-table";
import PatientUpdateForm from "./components/patient-update-form";
import PatientMedicalPlanForm from "./components/patient-medical-plan-form";
import {Redirect} from "react-router-dom";



class PatientContainer extends React.Component {
    dataInfo = [];
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloaded = this.reloaded.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.validate = false;
        this.validate2 = false;
        this.info = {
            id: null,
            dataMed: []
        }
        this.state = {
            id: "",
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPatient();
    }

    fetchPatient() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    handleUpdateById(id) {
        return API_USERS.getPatientById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully get patient with id: " + id);
                this.dataInfo[0] = result.id;
                this.dataInfo[1] = result.name;
                this.dataInfo[2] = result.address;
                this.dataInfo[3] = result.birthdate;
                this.dataInfo[4] = result.gender;
                this.dataInfo[5] = result.username;
                this.dataInfo[6] = result.password;
                this.dataInfo[7] = result.medicalRecord;
                this.dataInfo[8] = result.medicationPlans;

            }
            else console.log("failed with id: "+ id);
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatient();
    }

    reload() {
        this.setState({
            isLoaded: false
        });

        this.toggleForm();
        this.fetchPatient();
    }

    reloaded() {
        this.setState({
            isLoaded: false
        });
        this.fetchPatient();
    }

    handleById = (id) => {
        this.toggleForm();
        this.state.id = id;
        this.validUpdate();
        this.handleUpdateById(id);
        console.log("This is the id: " + id);

    }

    validUpdate(){
        this.validate=true;
    }

    handleMedical = (a) => {
        this.toggleForm();
        this.info.id = a[0];
        this.info.dataMed = a[1];
        console.log("Id-> "+ a[0]);
        console.log("MED-> "+ a[1]);

        this.validate2=true;
    }


    render() {
        if(localStorage.getItem('account') === "doctor") {
        return (
            <div>
                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <br/>

                    <Row>
                        <Col sm={{size: '9.5', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData}  idValue = {this.handleById} info = {this.handleMedical} reloadHandler = {this.reloaded} />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            /> }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> {(this.validate2)? "Add Medical Plan" : (this.validate)? "Update Patient" : "Add Patient" } : </ModalHeader>
                    <ModalBody>
                        {(this.validate2)? <PatientMedicalPlanForm reloadHandler={this.reload} id = {this.info.id} dataMed = {this.info.dataMed} />: (this.validate)?<PatientUpdateForm reloadHandler={this.reloadUpdate} id = {this.state.id} dataInfo = {this.dataInfo} /> : <PatientForm reloadHandler={this.reload}/> }
                        {this.validate = false}
                        {
                            this.validate2 = false}



                    </ModalBody>
                </Modal>

            </div>
        )
        }else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }

    }


}


export default PatientContainer;
