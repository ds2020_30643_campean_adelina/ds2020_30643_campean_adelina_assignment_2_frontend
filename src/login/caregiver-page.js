import React from 'react';


import {
    Button, Col,
    Input, ModalBody, Row
} from 'reactstrap';

import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Redirect} from "react-router-dom";
import Table from "../commons/tables/table";
import Stomp from "stompjs";

import SockJsClient from 'react-stomp';

import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';

//import SockJS from 'sockjs-client-ws';

//import SockJSClientWS  from 'react-stomp-client';

import ReactNotification from 'react-notifications-component'
import  store  from 'react-notifications-component';

import { useToasts } from 'react-toast-notifications'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'


let stomp = null;
toast.configure();
class CaregiverPage extends React.Component {


    constructor(props) {
        super(props);
        this.value = props;
        this.message = null;
        this.patientId = null;
        this.exists = false;
        this.caregiver = {
/*
            name: props.location.state.caregiver.name,
            birthdate: props.location.state.caregiver.birthdate,
            gender: props.location.state.caregiver.gender,
            address: props.location.state.caregiver.address,
            username: props.location.state.caregiver.username,
            password: props.location.state.caregiver.password,
            listOfPatients: props.location.state.caregiver.listOfPatients*/
            name:null,
            birthdate: null,
            gender: null,
            address: null,
            username: null,
            password: null,
            listOfPatients: null
        }

        console.log("caregiver first list : "+ this.caregiver.listOfPatients);

        //this.connect1();

    }
    // this.handleSubmit = this.handleSubmit.bind(this);

    columns = [
        {
            Header: 'Name',
            accessor: 'name',
            width: 160
        },
        {
            Header: 'Address',
            accessor: 'address',
            width: 160
        },
        {
            Header: 'Birthdate',
            accessor: 'birthdate',
            width: 160
        },
        {
            Header: 'Gender',
            accessor: 'gender',
            width: 160
        },
        {
            Header: 'Username',
            accessor: 'username',
            width: 160
        },
        {
            Header: 'Password',
            accessor: 'password',
            width: 160
        },
        {
            Header: 'MedicalRecord',
            accessor: 'medicalRecord',
            width: 160
        }];
    filters = [
        {
            accessor: 'name',
        }
    ];


    notify(not) {
        toast(not)
    }

    render() {

        if(localStorage.getItem('account') ==="caregiver") {

            this.caregiver.name = this.value.location.state.caregiver.name;
            this.caregiver.birthdate= this.value.location.state.caregiver.birthdate;
            this.caregiver.gender= this.value.location.state.caregiver.gender;
            this.caregiver.address= this.value.location.state.caregiver.address;
            this.caregiver.username= this.value.location.state.caregiver.username;
            this.caregiver.password= this.value.location.state.caregiver.password;
            this.caregiver.listOfPatients= this.value.location.state.caregiver.listOfPatients;


            for (let i = 0; i < this.caregiver.listOfPatients.length; i++) {
                if("f060d306-cc95-4a2a-ab01-d352a989021c" === this.caregiver.listOfPatients[i].id) {
                    this.exists = true;
                    break;
                }
            }

            const notify = () => {
                toast("Adelina has exceeded her "+this.message+" schedule!!")
            }
            return (



                <div>

                    {(this.exists) ? <SockJsClient url='http://localhost:8080/caregiver-websocket'
                                                   topics={['/caregiver/notification']}
                                                   onMessage={(notification) => {
                                                       console.log(notification);
                                                       this.patientId = notification.patient_id;
                                                       this.message = notification.activity_label;

                                                       // this.notificationMessage(notification);
                                                       //toast(notification);
                                                       // ToastDemo(notification);
                                                       notify();
                                                   }}
                    /> : ""
                    }


                    <div className="logpatient">
                        <h1 className="pinfo">Caregiver Information </h1>

                        <div className="logout-patient">

                            <a href="/login">LOGOUT</a>
                        </div>
                    </div>

                    <div className="tableClass">
                        <Table className="tableDetails"
                               data={this.caregiver.listOfPatients}
                               columns={this.columns}
                               search={this.filters}
                               pageSize={5}

                        />

                    </div>

                </div>

            )
        }else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }
    }


}

toast.configure();
function Message(not){

    const notify = (not) => {
        toast(not)
    }

    return (
        <div className="App">
            <button onClick={notify}>Notifications</button>
        </div>
    )
}

/*
function Home(not) {
    //const handleNotification = (not) => {
        store.addNotification({
            title: "Wonderful!",
            message: not,
            type: "success",
            insert: "top",
            container: "top-right",
            animationIn: ["animate__animated", "animate__fadeIn"],
            animationOut: ["animate__animated", "animate__fadeOut"],
            dismiss: {
                duration: 5000,
                onScreen: true
            }
        });
   //}
}*/

/*
export const ToastDemo = ({ content }) => {
    const { addToast } = useToasts()
    return (
        <Button onClick={() => addToast(content, {
            appearance: 'error',
            autoDismiss: true,
        })}>
            Add Toast
        </Button>
    )
}
*/
export default CaregiverPage;