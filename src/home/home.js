import React from 'react';

import BackgroundImg from '../commons/images/med11.jpg';
import projectStyle from '../commons/styles/project-style.css';
import {Button, Container, Jumbotron, NavLink} from 'reactstrap';
import {Redirect} from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "720px",
    backgroundImage: `url(${BackgroundImg})`,

};
const textStyle = {color: 'white', };

class Home extends React.Component {


    render() {

        if(localStorage.getItem('account') === "doctor") {
            return (

                <div style={backgroundStyle}>

                    <div className="doctorButtons">

                        <div className="flexClass">
                            <div className="logout">

                                <a href="/login">LOGOUT</a>
                            </div>

                            <div className="patientButton">

                                <a className="patientButton" href="/doctor/patient">Patient</a>
                            </div>
                        </div>
                        <div className="medicationButton">

                            <a className="medicationButton" href="/doctor/medication">Medication</a>
                        </div>

                        <div className="caregiverButton">

                            <a className="caregiverButton" href="/doctor/caregiver">Caregiver</a>
                        </div>


                    </div>

                </div>
            )
        }
        else {

            return (<Redirect to={{
                pathname: '/login'}}/>);
        }
    }

}


export default Home;